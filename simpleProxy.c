#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <ctype.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <fcntl.h>

#define SERVER_PORT 7878
#define MAXLINE 15000

typedef struct{
    unsigned char ip[4];
}IP;

int main(int argc, char const *argv[]) {
    int sockfd, browserSockFd, chilen, childpid, ser_port;
    struct sockaddr_in cli_addr, serv_addr;

    if( argv[1] != NULL ){
        ser_port = atoi( argv[1] );
    }
    else{
        ser_port = SERVER_PORT;
    }

    FILE* conf = fopen( "socks.conf", "r" );
    char* line = (char*)malloc( sizeof(char) * MAXLINE );
    char* socks_conf[10];
    for( int i = 0; i < 10; i++ ){
        socks_conf[i] = (char*)malloc( sizeof(char) * MAXLINE );
    }
    char readCh;
    int lineNum = 0;
    size_t len;

    while( ( readCh = getline( &line, &len, conf) ) != -1 ){
        strcpy( socks_conf[lineNum], line );
        lineNum++;
    }
    fclose( conf );
    IP conf_ip[lineNum];

    char** tokens = (char**)malloc( 30 * sizeof(char*) );
    char* token;
    int pos;
    // int conf_ip_idx = 0;
    for( int i = 0; i < lineNum; i++ ){
        pos = 0;
        token = strtok( socks_conf[i], "\r\n." );
        while( token != NULL ){
            tokens[pos] = token;
            pos++;
            token = strtok( NULL, "\r\n." );
        }
        tokens[pos] = NULL;

        for( int j = 0; j < pos; j++ ){
            if( strcmp( tokens[j], "*" ) != 0 ){
                conf_ip[i].ip[j] = atoi( tokens[j] );
            }
            else{
                conf_ip[i].ip[j] = 0;
            }
        }

    }

    /* Open a TCP socket ( an Internet stream socket ). */
    if( ( sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
        perror("server: can't open stream socket");
    }

    /* Bind local address so that the client can send to us. */
    bzero(( char* ) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl( INADDR_ANY );
    serv_addr.sin_port = htons( ser_port );

    if( bind( sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0 ){
        perror("server: can't bind local address");
    }

    listen(sockfd, 5);

    for(;;){
        chilen = sizeof( cli_addr );
        browserSockFd = accept( sockfd, (struct sockaddr*) &cli_addr, &chilen );

        if( browserSockFd < 0 )
        {
            perror("server: accept error");
            exit(-1);
        }

        socklen_t len;
        struct sockaddr_storage addr;
        char sourceIp[16];
        int source_port;

        len = sizeof( addr );
        getpeername( browserSockFd, (struct sockaddr*)&addr, &len );

        if( addr.ss_family == AF_INET ){
            struct sockaddr_in *s = (struct sockaddr_in *)&addr;
            source_port = ntohs( s->sin_port );
            inet_ntop( AF_INET, &s->sin_addr, sourceIp, sizeof(sourceIp) );
        }

        // printf( "IP address: %s\n", sourceIp );
        // printf( "Port: %d\n", source_port );


        if( (childpid = fork()) < 0 ) perror("server: fork error");
        else if( childpid == 0 ){

            /* close original socket */
            close( sockfd );

            unsigned char reqBuf[30000], replyBuf[30000];
            memset( reqBuf, 0, sizeof(reqBuf) );
            memset( replyBuf, 0, sizeof(replyBuf) );

            int len = read( browserSockFd, reqBuf, sizeof(reqBuf) );

            /* request */
            unsigned char VN = reqBuf[0];
            unsigned char CD = reqBuf[1];
            unsigned int DST_PORT = reqBuf[2] << 8 | reqBuf[3];
            unsigned int DST_IP = reqBuf[7] << 24 | reqBuf[6] << 16 | reqBuf[5] << 8 | reqBuf[4];
            char* USER_ID = reqBuf + 8;

            // print server infomation


            if( VN != 0x04 ) exit(0);

            /* connect mode */
            if( CD == 0x01 ){
                /* connect to remote server */
                int remote_fd;
                struct sockaddr_in remote_sin;
                struct hostent *he;
                printf( "<S_IP>\t:%s\n", sourceIp );
                printf( "<S_PORT>\t:%d\n", source_port );
                printf( "<D_IP>\t:%d.%d.%d.%d\n", reqBuf[4], reqBuf[5], reqBuf[6], reqBuf[7] );
                printf( "<D_IP>\t:%d\n", DST_PORT );
                printf( "<Command>\t:CONNECT\n" );

                remote_fd = socket( AF_INET, SOCK_STREAM, 0 );
                bzero( &remote_sin, sizeof(remote_sin) );
                remote_sin.sin_family = AF_INET;
                remote_sin.sin_addr.s_addr = DST_IP;
                remote_sin.sin_port = htons( DST_PORT );

                // check the ip
                int flag = 0;
                for( int i = 0; i < lineNum; i++ ){
                    // flag = 0;
                    for( int j = 0; j < 4; j++ ){
                        printf( "conf:%hu\treqBuf:%hu\n", conf_ip[i].ip[j], reqBuf[j + 4] );
                        if( conf_ip[i].ip[j] != 0 ){
                            if( conf_ip[i].ip[j] == reqBuf[j + 4] ){
                                flag = 1;
                            }
                            else{
                                flag = 0;
                                break;
                            }
                        }
                        else{
                            flag = 1;
                        }
                    }

                    if( flag == 1 ){
                        replyBuf[1] = 0x5A;
                        break;
                    }
                    else{
                        continue;
                        // replyBuf[1] = 0x5B;
                    }
                }

                if( flag == 0 ){
                    replyBuf[1] = 0x5B;
                }

                if( connect( remote_fd, (struct sockaddr *)&remote_sin, sizeof(remote_sin)) == -1 ){
                    replyBuf[1] = 0x5B;
                    perror( "connect failed");
                    // exit(1);
                }

                /* reply */
                replyBuf[0] = 0;
                replyBuf[2] = reqBuf[2];
                replyBuf[3] = reqBuf[3];
                replyBuf[4] = reqBuf[4];
                replyBuf[5] = reqBuf[5];
                replyBuf[6] = reqBuf[6];
                replyBuf[7] = reqBuf[7];

                write( browserSockFd, replyBuf, 8 );


                if( replyBuf[1] == 0x5A ){
                    printf( "<Reply>\t:Accept\n");
                }
                else if ( replyBuf[1] == 0x5B){
                    printf( "<Reply>\t:Reject\n");
                }

                if( replyBuf[1] == 0x5B ){
                    close( browserSockFd );
                    close( remote_fd );
                    exit(1);
                }

                fd_set rfds, afds;
                int nfds;
                // nfds = ( ( remote_fd > browserSockFd) ? remote_fd:browserSockFd ) + 1;
                nfds = FD_SETSIZE;
                FD_ZERO( &rfds );
                FD_ZERO( &afds );
                FD_SET( browserSockFd, &afds );
                FD_SET( remote_fd, &afds );

                char buffer1[15000], buffer2[15000];
                int cmd, rst, browser_end = 0, dst_end = 0;
                while(1) {
                    memcpy( &rfds, &afds, sizeof(rfds) );
                    if( select(nfds, &rfds, NULL, NULL, NULL) < 0 ) {
                        printf( "select error\n" );
                        exit( 1 );
                    }

                    if( FD_ISSET( browserSockFd, &rfds ) ) {
                        memset( buffer1, '\0', sizeof( buffer1 ) );
                        cmd = read( browserSockFd, buffer1, sizeof( buffer1 ) );
                        if( cmd <= 0 ){
                            FD_CLR( browserSockFd, &afds );
                            close( browserSockFd );
                            close( remote_fd );
                            break;
                        }
                        else{
                            printf( "<Content>\t:%s\n", buffer1 );
                            write( remote_fd, buffer1, cmd );
                        }
                    }

                    if( FD_ISSET( remote_fd, &rfds ) ) {
                        memset( buffer2, '\0', sizeof(buffer2) );
                        rst = read( remote_fd, buffer2, sizeof(buffer2) );

                        if( rst <= 0 ){
                            FD_CLR( remote_fd, &afds );
                            close( remote_fd );
                            close( browserSockFd );
                            break;
                        }
                        else{
                            printf( "<Content>\t:%s\n", buffer2 );
                            write( browserSockFd, buffer2, rst );
                        }
                    }
                } //end while
            }
            else if( CD == 0x02 ){ /* bind mode */
                int bind_fd, bind_port;
                struct sockaddr_in bind_sin;
                struct hostent *he;
                printf( "<S_IP>\t:%s\n", sourceIp );
                printf( "<S_PORT>\t:%d\n", source_port );
                printf( "<D_IP>\t:%d.%d.%d.%d\n", reqBuf[4], reqBuf[5], reqBuf[6], reqBuf[7] );
                printf( "<D_IP>\t:%d\n", DST_PORT );
                printf( "<Command>\t:BIND\n" );

                if( ( bind_fd = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 ) exit(1);
                bzero( ( char* )&bind_sin, sizeof( bind_sin ) );
                bind_sin.sin_family = AF_INET;
                bind_sin.sin_addr.s_addr = htonl( INADDR_ANY );
                bind_sin.sin_port = htons( INADDR_ANY );

                if( bind( bind_fd, (struct sockaddr*)&bind_sin, sizeof(bind_sin) ) < 0 ){
                    perror( "bind_fd: can't bind local address" );
                    exit(1);
                }

                struct sockaddr_in local_addr;
                int local_addrLen = sizeof( local_addr );
                unsigned char reply[8] = {0};
                bzero(( char* )&local_addr, sizeof( local_addr ) );
                if( getsockname( bind_fd, (struct sockaddr*)&local_addr, ( socklen_t* )&local_addrLen ) < 0 ){
                    perror( "can't get sock name");
                    reply[1] = 0x5B;
                }
                else{
                    reply[1] = 0x5A;
                }

                listen( bind_fd, 5 );

                reply[0] = 0;
                reply[2] = (unsigned char)( ntohs(local_addr.sin_port) / 256 );
                // reply[2] = (unsigned char)( ntohs( bind_sin.sin_port ) / 256 );
                reply[3] = (unsigned char)( ntohs(local_addr.sin_port) % 256 );
                // reply[3] = (unsigned char)( ntohs( bind_sin.sin_port ) % 256 );
                for( int i = 4; i < 8; i++ ){
                    reply[i] = 0;
                }

                write( browserSockFd, reply, 8 );

                if( replyBuf[1] == 0x5A ){
                    printf( "<Reply>\t:Accept\n");
                }
                else{
                    printf( "<Reply>\t:Reject\n");
                }

                /* accept connection from ftp */
                int ftp_fd, ftp_addrLen;
                struct sockaddr_in ftp_addr;
                ftp_addrLen = sizeof( ftp_addr );
                if( ( ftp_fd = accept( bind_fd, (struct sockaddr*)&ftp_addr, (socklen_t*)&ftp_addrLen ) ) < 0 ){
                    perror( "ftp accept error");
                    exit(1);
                }

                write( browserSockFd, reply, 8 );

                fd_set rfds, afds;
                int nfds;
                // nfds = ( ( ftp_fd > browserSockFd) ? ftp_fd:browserSockFd ) + 1;
                nfds = FD_SETSIZE;
                FD_ZERO( &rfds );
                FD_ZERO( &afds );
                FD_SET( browserSockFd, &afds );
                FD_SET( ftp_fd, &afds );

                char buf1[15000], buf2[15000];
                int browserLen, rst, browser_end = 0, ftp_end = 0;
                while(1) {
                    memcpy( &rfds, &afds, sizeof(rfds) );
                    if( select(nfds, &rfds, NULL, NULL, NULL) < 0 ) {
                        printf( "select error\n" );
                        exit( 1 );
                    }

                    if( FD_ISSET( browserSockFd, &rfds ) ) {
                        memset( buf1, '\0', sizeof( buf1 ) );
                        browserLen = read( browserSockFd, buf1, sizeof( buf1 ) );
                        printf( "<Content>\t:%s\n", buf1 );
                        if( browserLen <= 0 ){
                            FD_CLR( browserSockFd, &afds );
                            close( browserSockFd );
                            close( ftp_fd );
                            break;
                        }
                        else{
                            write( ftp_fd, buf1, browserLen );
                        }
                    }

                    if( FD_ISSET( ftp_fd, &rfds ) ) {
                        memset( buf2, '\0', sizeof(buf2) );
                        rst = read( ftp_fd, buf2, sizeof(buf2) );

                        if( rst <= 0 ){
                            FD_CLR( ftp_fd, &afds );
                            close( ftp_fd );
                            close( browserSockFd );
                            break;
                        }
                        else{
                            printf( "<Content>\t:%s\n", buf2 );
                            write( browserSockFd, buf2, rst );
                        }
                    }
                } //end while
            }

            close(browserSockFd);
            exit(1);
        }
        else{
            close( browserSockFd );
        }
    }
    close( sockfd );

    return 0;
}
