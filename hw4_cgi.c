#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <errno.h>

#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3

int gSc[5] = {0};
int recv_msg( int from, int colnum );
int readline( int fd, char *ptr, int maxlen, int i );

typedef struct{
    char* ip;
    char* port;
    char* file;
    char* sockHost;
    char* sockPort;
}Form;

Form formElement[5];

int main( int argc, char *argv[], char *envp[] )
{
    fd_set readfds; /* readable file descriptors */
    fd_set writefds; /* writable file descriptors */
    fd_set rs; /* active file descriptors */
    fd_set ws; /* active file descriptors */
    int nfds;
    // int    client_fd;
    // struct sockaddr_in client_sin;
    int client_fd[5];
    int conn = 0;
    extern int errno;
    struct sockaddr_in client_sin[5];
    char msg_buf[30000];
    int len, first;
    // int SERVER_PORT;
    int server_port[5] = {0};
    FILE *fp[5] = {0};
    int flag[5] = {0};
    int status[5];
    // FILE *fp;
    int end;
    // struct hostent *he;
    struct hostent *he;
    // gSc = 0;

    for( int i = 0; i < 5; i++ ){
        status[i] = -1;
        client_fd[i] = -1;
    }


    printf( "Content-type: text/html\n\n" );
    printf( "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />" );
    // int i;
    // char *query;
    // for(i = 0; envp[i] != NULL; ++i ){
    //     printf("%s<br/>", envp[i]);
    // }


    char* query_str = getenv( "QUERY_STRING" );

    char** tokens = (char**)malloc( 30 * sizeof(char*) );
    for( int i = 0; i < 30; i++ ){
        tokens[i] = NULL;
    }
    char* token;
    int pos = 0;

    token = strtok( query_str, "&" );
    while( token != NULL ){
        tokens[pos] = token;
        pos++;
        token = strtok( NULL, "&" );
    }
    tokens[pos] = NULL;

    char** tmp = (char**)malloc( 30 * sizeof(char*) );
    char* lalala;

    for( int i = 0; i < 5; i++ ){
        formElement[i].ip = (char*)malloc( sizeof(char) * 2048 );
        formElement[i].port = (char*)malloc( sizeof(char) * 2048 );
        formElement[i].file = (char*)malloc( sizeof(char) * 2048 );
        formElement[i].sockHost = (char*)malloc( sizeof(char) * 2048 );
        formElement[i].sockPort = (char*)malloc( sizeof(char) * 2048 );
    }


    for( int i = 0; i < 25; i++ ){
        pos = 0;
        lalala = strtok( tokens[i], "=" );
        while( lalala != NULL ){
            tmp[pos] = lalala;
            pos++;

            lalala = strtok( NULL, "=" );
        }
        tmp[pos] = NULL;

        if( i % 5 == 0 ){
            if( pos == 1 ){
                strcpy( formElement[i/5].ip, " " );
            }
            else{
                strcpy( formElement[i/5].ip, tmp[1] );
            }
        }
        else if( i % 5 == 1 ){
            if( pos == 1 ){
                strcpy( formElement[i/5].port, " " );
            }
            else{
                strcpy( formElement[i/5].port, tmp[1] );
            }
        }
        else if( i % 5 == 2 ){
            if( pos == 1 ){
                strcpy( formElement[i/5].file, " " );
            }
            else{
                strcpy( formElement[i/5].file, tmp[1] );
            }
        }
        else if( i % 5 == 3 ){
            if( pos == 1 ){
                strcpy( formElement[i/5].sockHost, " " );
            }
            else{
                strcpy( formElement[i/5].sockHost, tmp[1] );
            }
        }
        else{
            if( pos == 1 ){
                strcpy( formElement[i/5].sockPort, " " );
            }
            else{
                strcpy( formElement[i/5].sockPort, tmp[1] );
            }
        }

        for( int j = 0; j < 30; j++ ){
            tmp[j] = NULL;
        }
    }

    // for( int i = 0; i < 15; i++ ){
    //     if( i % 3 == 0 ){
    //         strcpy( formElement[i/3].ip, argv[i] );
    //     }
    //     else if( i % 3 == 1 ){
    //         strcpy( formElement[i/3].port, argv[i] );
    //     }
    //     else{
    //         strcpy( formElement[i/3].file, argv[i] );
    //     }
    // }

    printf( "<title>Network Programming Homework 3</title>\n" );
    printf( "<body bgcolor=#336699>\n" );
    printf( "<font face=\"Courier New\" size=2 color=#FFFF99>\n" );
    printf( "<table width=\"800\" border=\"1\">\n" );
    printf( "<tr>\n" );
    for( int i = 0; i < 5; i++ ){
        if( strcmp( formElement[i].ip, " " ) != 0 ){
            printf( "<td>" );
            printf( "%s", formElement[i].ip );
            printf( "</td>" );
            conn++;
        }
    }
    printf( "</tr>\n" );
    printf( "<tr>\n" );
    printf( "<td valign=\"top\" id=\"m0\"></td><td valign=\"top\" id=\"m1\"></td><td valign=\"top\" id=\"m2\"></td><td valign=\"top\" id=\"m3\"></td><td valign=\"top\" id=\"m4\"></td></tr>\n" );
    printf( "</table>\n");

    unsigned char request[5][9];
    for( int i = 0; i < 5; i++ ){
        for( int j = 0; j < 9; j++ ){
            request[i][j] = 0;
        }
    }


    for( int i = 0; i < 5; i++ ){
        if( strcmp( formElement[i].file, " " ) != 0 ){
            fp[i] = fopen( formElement[i].file, "r" );
            if( fp[i] == NULL ){
                fprintf(stderr,"Error : '%s' doesn't exist\n", formElement[i].file);
                exit(1);
            }
        }

        // if( strcmp( formElement[i].ip, " " ) != 0 ){
        //     if( ( he = gethostbyname( formElement[i].ip ) ) == NULL ){
        //         fprintf(stderr,"Usage : client <server ip> <port> <testfile>");
        //         exit(1);
        //     }
        //     server_port[i] = (u_short)atoi( formElement[i].port );
        //
        //     client_fd[i] = socket( AF_INET, SOCK_STREAM, 0 );
        //     bzero( &client_sin[i], sizeof( client_sin[i] ) );
        //     client_sin[i].sin_family = AF_INET;
        //     client_sin[i].sin_addr = *( (struct in_addr *)he->h_addr );
        //     client_sin[i].sin_port = htons( server_port[i] );
        // }

        if( strcmp( formElement[i].ip, " " ) != 0 ){
            if( ( he = gethostbyname( formElement[i].sockHost ) ) == NULL ){
                fprintf(stderr,"Usage : client <server ip> <port> <testfile>");
                exit(1);
            }
            server_port[i] = (u_short)atoi( formElement[i].sockPort );

            client_fd[i] = socket( AF_INET, SOCK_STREAM, 0 );
            bzero( &client_sin[i], sizeof( client_sin[i] ) );
            client_sin[i].sin_family = AF_INET;
            client_sin[i].sin_addr = *( (struct in_addr *)he->h_addr );
            client_sin[i].sin_port = htons( server_port[i] );

            struct hostent *server;
            server = gethostbyname( formElement[i].ip );
            unsigned long ip = (*((struct in_addr *)server->h_addr)).s_addr;

            // fill in the request package
            request[i][0] = 0x04;
            request[i][1] = 0x01;
            request[i][2] = atoi( formElement[i].port ) / 256;
            request[i][3] = atoi( formElement[i].port ) % 256;
            request[i][7] = ip >> 24;
            request[i][6] = (ip >> 16) & 0xFF;
            request[i][5] = (ip >> 8) & 0xFF;
            request[i][4] = ip & 0xFF;

        }
    }

    unsigned char reply[5][9];
    for( int i = 0; i < 5; i++ ){
        for( int j = 0; j < 9; j++ ){
            reply[i][j] = 0;
        }
    }


    for( int i = 0; i < 5; i++ ){
        if( client_fd[i] != -1 ){
            flag[i] = fcntl( client_fd[i], F_GETFL, 0 );
            fcntl( client_fd[i], F_SETFL, flag[i]|O_NONBLOCK );
            if( connect( client_fd[i], (struct sockaddr *)&client_sin[i], sizeof(client_sin[i])) < 0 ){
                // perror("");
                // exit(1);
                write( client_fd[i], request, 8 );
                sleep(1);
                read( client_fd[i], reply[i], 8 );
                if( reply[i][1] == 0x5B ){
                    close( client_fd[i] );
                    client_fd[i] = -1;
                }
                if( errno != EINPROGRESS ){
                    return(-1);
                }
                perror("bye\n");
                errno = 0;
            }
        }
    }



    nfds = FD_SETSIZE;
    FD_ZERO( &readfds );
    FD_ZERO( &writefds );
    FD_ZERO( &rs );
    FD_ZERO( &ws );


    for( int i = 0; i < 5; i++ ){
        if( client_fd[i] != -1 ){
            FD_SET( client_fd[i], &rs );
            FD_SET( client_fd[i], &ws );
            // FD_SET( fileno( fp[i] ),&readfds );
            status[i] = F_CONNECTING;
        }
    }

    readfds = rs;
    writefds = ws;

    int whatever;
    socklen_t whateverlen = sizeof(int);

    sleep(1);     //waiting for welcome messages

    end = 0;
    // for( int j = 0; j < 7; j++ )
    while( conn > 0 )
    {
        // FD_ZERO(&readfds);
        // FD_SET(client_fd,&readfds);
        // if( end == 0 )
        //     FD_SET( fileno(fp),&readfds );
        memcpy( &readfds, &rs, sizeof(readfds) );
        memcpy( &writefds, &ws, sizeof(writefds) );
        if( select( nfds, &readfds, &writefds, NULL, NULL ) < 0 )
            exit(1);

        for( int i = 0; i < 5; i++ ){
            if( status[i] == F_CONNECTING && ( FD_ISSET( client_fd[i], &readfds ) || ( FD_ISSET( client_fd[i], &writefds ) ) ) ){
                if( getsockopt( client_fd[i], SOL_SOCKET, SO_ERROR, &whatever, &whateverlen ) < 0 || whatever != 0 ){
                    return( -1 );
                }
                perror("hi\n");
                status[i] = F_READING;
                FD_CLR( client_fd[i], &ws );
            }
            else if( status[i] == F_WRITING && FD_ISSET( client_fd[i], &writefds ) && gSc[i] == 1){
                char tmpmsg[3000];
                int writeLen = 0;
                memset( tmpmsg, 0, 3000 );
                len = readline( fileno( fp[i] ), msg_buf, sizeof(msg_buf), i );
                if(len < 0) exit(1);

                msg_buf[len - 1] = 13;
                msg_buf[len] = 10;

                msg_buf[len + 1] = '\0';

                printf( "<script>document.all['m" );
                printf( "%d'].innerHTML += \"", i );

                printf( "<b>" );
                for( int i = 0; i < len; i++ ){
                    if( msg_buf[i] != '\n' &&  msg_buf[i] != '\r' ){
                        printf( "%c", msg_buf[i] );
                    }
                }
                printf( "</b><br>\";</script>" );
                // printf( "<b>%s</b><br>\";</script>", tmpmsg );
                fflush( stdout );
                if( ( writeLen = write( client_fd[i], msg_buf, len + 1 ) ) == -1 ) return -1;
                // writeLen = write( client_fd[i], msg_buf, len + 1 );
                // len -= writeLen;
                // printf( "hell:%d\n", writeLen );
                // printf( "shit:%d\n", len + 1 );
                // if( writeLen <= 0 || len <= 0 ){
                    //write finished
                    gSc[i] = 0;
                    FD_CLR( client_fd[i], &ws );
                    status[i] = F_READING;
                    FD_SET( client_fd[i], &rs );
                // }
            }
            else if( status[i] == F_READING && FD_ISSET( client_fd[i], &readfds ) ){
                int errnum;
                errnum = recv_msg( client_fd[i], i );

                if( errnum <= 0)
                {
                    shutdown( client_fd[i],2 );
                    close( client_fd[i] );
                    FD_CLR( client_fd[i], &rs );
                    status[i] = F_DONE;
                    conn--;
                    // exit(1);
                }
                else if( errnum > 0 && gSc[i] == 1 ){
                    FD_CLR( client_fd[i], &rs );
                    status[i] = F_WRITING;
                    FD_SET( client_fd[i], &ws );
                }
                // else if (errnum ==0){
                //     shutdown( client_fd[i],2 );
                //     close( client_fd[i] );
                //     exit(0);
                // }
            }
        }
        // if( gSc ==1 )
        // {
        //     //�emeesage
        //     char tmpmsg[3000];
        //     memset( tmpmsg, 0, 3000 );
        //     len = readline(fileno(fp),msg_buf,sizeof(msg_buf));
        //     if(len < 0) exit(1);
        //
        //     msg_buf[len-1] = 13;
        //     msg_buf[len] = 10;
        //
        //     msg_buf[len+1] = '\0';
        //     // char* tmpmsg = (char*)malloc( len );
        //     // strcpy( tmpmsg, msg_buf );
        //     // tmpmsg[len-1] = '\0';
        //     printf( "<b>" );
        //     for( int i = 0; i < len; i++ ){
        //         if( msg_buf[i] != '\n' &&  msg_buf[i] != '\r' ){
        //             printf( "%c", msg_buf[i] );
        //         }
        //     }
        //     printf( "</b><br>\";</script>" );
        //     // printf( "<b>%s</b><br>\";</script>", tmpmsg );
        //     fflush( stdout );
        //     if(write(client_fd,msg_buf,len+1) == -1) return -1;
        //
        //     /*if(!strncmp(msg_buf,"exit",4))
        //     {
        //     sleep(2);	//waiting for server messages
        //     while(recv_msg(client_fd) >0 )
        //     {}
        //     shutdown(client_fd,2);
        //     close(client_fd);
        //     fclose(fp);
        //     end =1;
        // }    */
        //     //usleep(1000);// delay 1000 microsecond
        //     gSc =0;
        // }
        // if(FD_ISSET(client_fd,&readfds))
        // {
        //     //����message
        //     int errnum;
        //     errnum =recv_msg(client_fd);
        //     if(errnum <0)
        //     {
        //         shutdown(client_fd,2);
        //         close(client_fd);
        //         exit(1);
        //     } else if (errnum ==0){
        //         shutdown(client_fd,2);
        //         close(client_fd);
        //         exit(0);
        //     }
        // }
    } // end of while

    for( int i = 0; i < 5; i++ ){
        free( formElement[i].ip );
        free( formElement[i].port );
        free( formElement[i].file );
        free( formElement[i].sockHost );
        free( formElement[i].sockPort );
    }
    return 0;
}  // end of main


int recv_msg( int from, int colnum )
{
    printf( "<script>document.all['m" );
    printf( "%d'].innerHTML += \"", colnum );
    char buf[3000];
    int len;
    if( ( len = readline( from, buf, sizeof(buf) - 1, colnum ) ) < 0 ) return -1;

    buf[len] = 0;

    for( int i = 0; i < len; i++ ){
        if( buf[i] == '>' ){
            printf( "&gt;" );
        }
        else if( buf[i] == '<' ){
            printf( "&lt;" );
        }
        else if( buf[i] == '\"' ){
            printf( "\\\"" );
        }
        else if( ( buf[i] == '\n' || buf[i] == '\r' ) && len > 1 ){
            buf[i] = '\0';
        }
        else if( len == 1 && buf[i] == '\n' ){
            printf( "<br>" );
        }
        else{
            printf( "%c", buf[i] );
        }
    }

    if( strstr( buf, "illegal tag" ) != NULL ){
        fprintf( stderr, "<script>document.all['m" );
        fprintf( stderr, "%d'].innerHTML += \"", colnum);
        fprintf( stderr, "%s", buf );
        fprintf( stderr, "<br>\";</script>" );
    }

    if( strncmp( buf, "% ", strlen("% ") ) != 0 ){
        printf( "<br>\";</script>" );
    }
    else{
        printf( "\";</script>" );
    }

    fflush(stdout);
    fflush(stderr);
    return len;
}

int readline(int fd,char *ptr,int maxlen, int i )
{
    int n,rc;
    char c;
    *ptr = 0;
    for(n=1;n<maxlen;n++)
    {
        if((rc=read(fd,&c,1)) == 1)
        {
            *ptr++ = c;
            if(c==' '&& *(ptr-2) =='%'){ gSc[i] = 1; break; }
            if(c=='\n')  break;
        }
        else if(rc==0)
        {
            if(n==1)     return(0);
            else         break;
        }
        else{
            if( errno == EAGAIN ){
                // errno = 0;
                // *ptr++ = c;
                // if(c==' '&& *(ptr-2) =='%'){ gSc[i] = 1; break; }
                // if(c=='\n')  break;
                // errno = 0;
                sleep(1);
                n--;
                continue;
            }

            return(-1);

            // return(-1);

        }
    }

    return(n);
}
