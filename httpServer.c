#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <ctype.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXLINE 15000
#define SERVER_PORT 7878
#define SHELL_TOK_BUFSIZE 64
#define SHELL_TOK_DELIM " \t\r\n"
#define MY_PATH "PATH=bin:."
#define DEFAULT_DIR "/home/np_project4_0556045"
#define OK_MSG "HTTP/1.1 200 OK\r\n"
#define SWAP( a, b ) { c = a; a = b; b = c; }
#define MAX_PIPE_NUM 2000

// typedef struct {
//     char* h;
//     char* p;
//     char* f;
// }Form;

void err_dump( const char* x );
char* formElement[16];

int main(int argc, char const *argv[]) {
    /* code */
    int sockfd, newsockfd, chilen, childpid, server_port;
    struct sockaddr_in cli_addr, serv_addr;

    if( argv[1] != NULL ){
        server_port = atoi( argv[1] );
    }
    else{
        server_port = SERVER_PORT;
    }

    /* enlarge the fd table */
    struct rlimit r;
    r.rlim_cur=2100;
    r.rlim_max=2500;

    if (setrlimit(RLIMIT_NOFILE,&r)<0){
        fprintf(stderr,"setrlimit error\n");
        exit(1);
    }

    /* Open a TCP socket ( an Internet stream socket ). */
    if( ( sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
        err_dump("server: can't open stream socket");
    }

    /* Bind local address so that the client can send to us. */
    bzero(( char* ) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl( INADDR_ANY );
    serv_addr.sin_port = htons( server_port );

    if( bind( sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0 ){
        err_dump("server: can't bind local address");
    }

    listen(sockfd, 5);

    for(;;){
        chilen = sizeof( cli_addr );
        newsockfd = accept( sockfd, (struct sockaddr*) &cli_addr, &chilen );
        if( newsockfd < 0 ) err_dump("server: accept error");


        if( (childpid = fork()) < 0 ) err_dump("server: fork error");
        else if( childpid == 0 ){
            /* child process */

            /* close original socket */
            close( sockfd );
            chdir( DEFAULT_DIR );
            chroot( DEFAULT_DIR );

            char buffer[MAXLINE];
            char** tokens = (char**)malloc( 30 * sizeof(char*) );
            char* token;
            char* getMesg = (char*)malloc( 4096 * sizeof(char) );
            char* filename = (char*)malloc( 1024 * sizeof(char) );
            char* barPar = (char*)malloc( 4096 * sizeof(char) );
            char* formData = (char*)malloc( 1024 * 15 * sizeof(char) );
            char* query_env = (char*)malloc( 4096 * sizeof(char) );

            for( int i = 0; i < 16; i++ ){
                // formElement[i].f = (char*)malloc( 2048 * sizeof(char) );
                // formElement[i].p = (char*)malloc( 2048 * sizeof(char) );
                // formElement[i].h = (char*)malloc( 2048 * sizeof(char) );
                formElement[i] = (char*)malloc( 2048 * sizeof(char) );
                memset( formElement[i], 0, 2048 );
            }
            int position = 0;

            if( ( read( newsockfd, buffer, MAXLINE ) ) < 1 ){
                exit(0);
            }

            token = strtok( buffer, "\r\n" );
            while( token != NULL ){
                tokens[position] = token;
                position++;

                token = strtok( NULL, "\r\n" );
            }
            tokens[position] = NULL;

            /* getMesg is the first line of GET /xxx .... */
            strcpy( getMesg, tokens[0] );

            for( int i = 0; i < 30; i++ ){
                tokens[i] = NULL;
            }
            free( token );


            position = 0;
            token = strtok( getMesg, " " );
            while( token != NULL ){
                tokens[position] = token;
                position++;

                token = strtok( NULL, " " );
            }
            tokens[position] = NULL;

            /* the parameter of the website bar */
            strcpy( barPar, tokens[1] );
            memmove( &barPar[0], &barPar[1], strlen(barPar) );

            for( int i = 0; i < 30; i++ ){
                tokens[i] = NULL;
            }
            free( token );
            tokens = (char**)malloc( 30 * sizeof(char*) );

            position = 0;
            token = strtok( barPar, "?" );
            while( token != NULL ){
                tokens[position] = token;
                position++;

                token = strtok( NULL, "?" );
            }
            tokens[position] = NULL;

            strcpy( filename, tokens[0] );
            if( tokens[1] != NULL ){
                strcpy( query_env, tokens[1] );
            }
            // write( STDOUT_FILENO, filename, strlen(filename) );



            /* get each parameter in the form */
            if( tokens[1] != NULL ){
                position = 0;
                strcpy( formData, tokens[1] );

                for( int i = 0; i < 30; i++ ){
                    tokens[i] = NULL;
                }
                free( token );
                tokens = (char**)malloc( 30 * sizeof(char*) );

                token = strtok( formData, "&" );
                while( token != NULL ){
                    tokens[position] = token;
                    position++;

                    token = strtok( NULL, "&" );
                }
                tokens[position] = NULL;

                char** tmp = (char**)malloc( 30 * sizeof(char*) );
                char* lalala;
                int pos;

                for( int i = 0; i < 15; i++ ){
                    pos = 0;
                    lalala = strtok( tokens[i], "=" );
                    while( lalala != NULL ){
                        tmp[pos] = lalala;
                        pos++;

                        lalala = strtok( NULL, "=" );
                    }
                    tmp[pos] = NULL;

                    // pos = 0;
                    // while( tmp[pos] != NULL ){
                    //     write( STDOUT_FILENO, tmp[pos], strlen(tmp[pos]) );
                    //     pos++;
                    // }
                    if( i % 3 == 0 ){
                        if( pos == 1 ){
                            strcpy( formElement[i], " " );
                        }
                        else{
                            strcpy( formElement[i], tmp[1] );
                        }
                    }
                    else if( i % 3 == 1 ){
                        if( pos == 1 ){
                            strcpy( formElement[i], " " );
                        }
                        else{
                            strcpy( formElement[i], tmp[1] );
                        }
                    }
                    else{
                        if( pos == 1 ){
                            strcpy( formElement[i], " " );
                        }
                        else{
                            strcpy( formElement[i], tmp[1] );
                        }
                    }

                    for( int j = 0; j < 30; j++ ){
                        tmp[j] = NULL;
                    }
                }
                formElement[15] = NULL;
                // for( int i = 0; i < 15; i++ ){
                //     // write( STDOUT_FILENO, formElement[i].h, strlen( formElement[i].h ) );
                //     // write( STDOUT_FILENO, formElement[i].p, strlen( formElement[i].p ) );
                //     // write( STDOUT_FILENO, formElement[i].f, strlen( formElement[i].f ) );
                //     write( STDOUT_FILENO, formElement[i], strlen(formElement[i]) );
                //     write( STDOUT_FILENO, "\n", 1 );
                // }

            }


            dup2( newsockfd, STDOUT_FILENO );
            dup2( newsockfd, STDERR_FILENO );

            write( STDOUT_FILENO, OK_MSG, strlen(OK_MSG) );

            FILE* html;
            int nread;
            char buf[MAXLINE];

            if( strcmp( filename, "form_get2.htm" ) == 0 ){
                write( STDOUT_FILENO, "Content-Type: text/html\n\n", strlen("Content-Type: text/html\n\n") );
                html = fopen( "form_get2.htm", "r" );
                if( html ){
                    while( fgets( buf, MAXLINE, html ) != NULL ){
                        write( STDOUT_FILENO, buf, strlen(buf) );
                    }
                }
                fclose( html );

            }
            else{
                int pid = fork();
                if( pid < 0 ){
                    err_dump( "fork error" );
                }
                else if( pid == 0 ){
                    char cgi[100];
                    memset( cgi, 0, 100 );
                    strcpy( cgi, "./" );
                    strcat( cgi, filename );

                    setenv( "QUERY_STRING", query_env, 1 );

                    if( execvp( cgi, formElement ) == -1 ){
                        err_dump( "execl failed" );
                        exit(0);
                    }
                }
                else{
                    wait(NULL);
                }
            }

            for( int i = 0; i < 16; i++ ){
                free( formElement[i] );
            }
            exit(0);
        }
        else{
            close( newsockfd ); /* parent process */
            wait( NULL );
        }
    }

    close( sockfd );
    return 0;
}


// print the error messages
void err_dump( const char* x ){
    perror(x);
    exit(1);
}
